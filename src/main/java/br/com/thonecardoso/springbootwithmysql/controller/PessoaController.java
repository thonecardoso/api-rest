package br.com.thonecardoso.springbootwithmysql.controller;

import br.com.thonecardoso.springbootwithmysql.controller.dto.PessoaRs;
import br.com.thonecardoso.springbootwithmysql.repository.PessoaRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    private final PessoaRepository pessoaRepository;

    public PessoaController(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }


    @GetMapping("/")
    public List<PessoaRs> findAll(){
        var pessoas = pessoaRepository.findAll();
        return pessoas
                .stream()//Estudas api stream
                .map(PessoaRs::converter)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public PessoaRs findById(@PathVariable("id") Long id){
        var pessoa = pessoaRepository.getOne(id);
        return PessoaRs.converter(pessoa);
    }


}
