package br.com.thonecardoso.springbootwithmysql.repository;

import br.com.thonecardoso.springbootwithmysql.model.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
}
