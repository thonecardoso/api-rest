package br.com.thonecardoso.springbootwithmysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.Entity;

@SpringBootApplication(scanBasePackages = "br.com.thonecardoso.springbootwithmysql")
@EntityScan(basePackages="br.com.thonecardoso.springbootwithmysql.model")
public class SpringBootWithMysqlApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringBootWithMysqlApplication.class, args);
	}

}
